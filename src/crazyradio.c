// USB driver for the Crazyradio USB dongle

#include <errno.h>
#include <signal.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <libusb.h>
#include <assert.h>

#include "crazyradio.h"

// USB parameters
#define CRADIO_VID 0x1915
#define CRADIO_PID 0x7777

// Dongle configuration requests
// See http://wiki.bitcraze.se/projects:crazyradio:protocol for documentation
#define	SET_RADIO_CHANNEL 0x01
#define	SET_RADIO_ADDRESS 0x02
#define	SET_DATA_RATE 0x03
#define	SET_RADIO_POWER 0x04
#define	SET_RADIO_ARD 0x05
#define	SET_RADIO_ARC 0x06
#define	ACK_ENABLE 0x10
#define	SET_CONT_CARRIER 0x20
#define	SCANN_CHANNELS 0x21
#define	LAUNCH_BOOTLOADER 0xFF
#define USB_TYPE_VENDOR 64

void radioAck_init(radioAck *ackPtr){
	ackPtr->ack = 0; //false
	ackPtr->powerDet = 0; //false
	ackPtr->retry = 0;
};



void Crazyradio_init(crazyradio *crazyradioPtr){
	crazyradioPtr->DR_250KPS = 0;
    crazyradioPtr->DR_1MPS = 1;
    crazyradioPtr->DR_2MPS = 2;

    crazyradioPtr->P_M18DBM = 0;
    crazyradioPtr->P_M12DBM = 1;
    crazyradioPtr->P_M6DBM = 2;
    crazyradioPtr->P_0DBM = 3;

    crazyradioPtr->handle = NULL;
}

uint16_t getDeviceById(libusb_device* dev){
	// get device list
	libusb_context *context = NULL;
    static libusb_device **list = NULL;
    int rc = 0;
    ssize_t count = 0;

    rc = libusb_init(&context);
    assert(rc == 0);

    count = libusb_get_device_list(context, &list);
    assert(count > 0);

    for (size_t idx = 0; idx < count; ++idx) {
        libusb_device *device = list[idx];
        struct libusb_device_descriptor desc = {0};

        rc = libusb_get_device_descriptor(device, &desc);
        assert(rc == 0);

        if(desc.idVendor == 0x1915 && desc.idProduct == 0x7777){
        	dev = device;
        	return desc.bcdDevice;
        }
    }
    return 0;
}


int Crazyradio_USB_init(crazyradio *crazyradioPtr, uint16_t devid){

	int res;
	res = libusb_init(NULL);
	if(res != 0){
		printf("initialization fail\n");
		return 0;
	}

	if(crazyradioPtr->handle == NULL) {
		//getDeviceById(crazyradioPtr->dev);
		crazyradioPtr->handle = libusb_open_device_with_vid_pid(NULL, 0x1915, 0x7777);
		if(crazyradioPtr->handle == NULL){
			printf("Cannot find a Crazyradio Dongle\n");
			return 0;
		}
	}
	printf("auto detaching ~~\n");
	// kernel driver handle
	libusb_set_auto_detach_kernel_driver(crazyradioPtr->handle, 1);    // enable auto detach
	// claim interface #0
	printf("finished auto detach!!!\n");
		
	res = libusb_claim_interface(crazyradioPtr->handle, 0);
	if(res != 0){
		errorReport(res);
		return 0;
	}else{
		printf("claim success!\n");
	}

	crazyradioPtr->version = getDeviceById(crazyradioPtr->dev); // returned bcdDevice

	if(crazyradioPtr->version == 0){
		printf("Cannot find the firmware version\n");
		return 0;
	}
	// version < 0.3
	if(crazyradioPtr->version < 3){
		printf("This driver requires Crazyradio firmware V0.3+\n");
		return 0;
	}
	// version < 0.4
	if (crazyradioPtr->version < 4){
		printf("You should update to Crazyradio firmware V0.4+\n");
	}

	//Reset the dongle to power up settings
	set_data_rate(crazyradioPtr, crazyradioPtr->DR_2MPS);
    set_channel(crazyradioPtr,2);
    crazyradioPtr->arc = -1;



    if(crazyradioPtr->version >= 0.4){
    	unsigned char address[5];
    	memset(address, 0xE7, 5 * sizeof(address[0]));
        set_cont_carrier(crazyradioPtr,0);
        set_address(crazyradioPtr, address);
        set_power(crazyradioPtr, crazyradioPtr->P_0DBM);
        set_arc(crazyradioPtr, 3);
        set_ard_bytes(crazyradioPtr, 32);
    }
    return 1;
}

void close_crazyradio(crazyradio *crazyradioPtr){
	// if pyusb1 is false
	if (crazyradioPtr->handle != NULL)
	{
		libusb_release_interface(crazyradioPtr->handle, 0);
		libusb_reset_device(crazyradioPtr->handle);
	}
	crazyradioPtr->handle = NULL;
}

void set_channel(crazyradio *crazyradioPtr, uint16_t channel){
	unsigned char packet[64];
	_send_vendor_setup(crazyradioPtr->handle, SET_RADIO_CHANNEL, channel, 0, packet, 64);
}

int set_address(crazyradio *crazyradioPtr, unsigned char *address){
	if(sizeof(*address) != 5){
		printf("razyradio: the radio address shall be 5\n");
		printf("bytes long\n");
		return 0;
	}
	_send_vendor_setup(crazyradioPtr->handle, SET_RADIO_ADDRESS, 0, 0, address, 5);
	return 1;
}

void set_data_rate(crazyradio *crazyradioPtr, uint16_t datarate){
	unsigned char packet[64];
	_send_vendor_setup(crazyradioPtr->handle, SET_DATA_RATE, datarate, 0, packet, 64);
}

void set_power(crazyradio *crazyradioPtr, uint16_t power){
	unsigned char packet[64];
	_send_vendor_setup(crazyradioPtr->handle, SET_RADIO_POWER, power, 0, packet,64);
}

void set_arc(crazyradio *crazyradioPtr, uint16_t arc){
	unsigned char packet[64];
	_send_vendor_setup(crazyradioPtr->handle, SET_RADIO_ARC, arc, 0, packet, 64);
	crazyradioPtr->arc = arc;
}

void set_ard_time(crazyradio *crazyradioPtr, uint16_t us){ //// us type??????????????
	unsigned char packet[64];
	uint16_t t;
	t = (int)((us / 250) - 1);
	if(t < 0)
		t=0;
	if(t > 0xF)
		t = 0xF;
	_send_vendor_setup(crazyradioPtr->handle, SET_RADIO_ARD, t, 0, packet, 64);
}

void set_ard_bytes(crazyradio *crazyradioPtr, uint16_t nbytes){
	unsigned char packet[64];
	_send_vendor_setup(crazyradioPtr->handle, SET_RADIO_ARD, 0x80 | nbytes, 0, packet,64);
}

void set_cont_carrier(crazyradio *crazyradioPtr, int active){
	unsigned char packet[64];
	if(active){
		_send_vendor_setup(crazyradioPtr->handle, SET_CONT_CARRIER, 1, 0, packet,64);
	}else{
		_send_vendor_setup(crazyradioPtr->handle, SET_CONT_CARRIER, 0, 0, packet,64);
	}
}

int _has_fw_scan(crazyradio *crazyradioPtr){
	return 0; //false
}

// Dongle configuration
void set_channels(crazyradio *crazyradioPtr, uint16_t channel){
	unsigned char packet[64];
	_send_vendor_setup(crazyradioPtr->handle, SET_RADIO_CHANNEL, channel,0, packet,64);
}

// inputted packet carries either input or output data
void scan_channels(crazyradio *crazyradioPtr, uint16_t start, uint16_t stop,
					unsigned char *packet, uint16_t wLength, unsigned char *array){
	if(_has_fw_scan(crazyradioPtr)){
		_send_vendor_setup(crazyradioPtr->handle, SCANN_CHANNELS, start,
							stop, packet, 64);
		_get_vendor_setup(crazyradioPtr->handle, SCANN_CHANNELS, 0, 0, array, 64);
	}else{ //slow PC-driven scan
		uint16_t result[32];
		int index = 0;
		radioAck status;
		radioAck_init(&status);
		for(int i=start; i<stop; i++){
			set_channels(crazyradioPtr, i);
			if(send_packet(crazyradioPtr, packet, &status)){
				result[index] = i;
				index++;
			}
		}
		memcpy(array, &result, 64); //copy 64 bytes
	}
}



// Data transferts
int send_packet(crazyradio *crazyradioPtr, unsigned char *dataOut,
				radioAck *ackIn){
	unsigned char data[64];
	int res = 0;
	int numBytes = 0;

	res = libusb_bulk_transfer(crazyradioPtr->handle, 0x01, dataOut, 7, &numBytes, 1000);
	if(0 != res){
		printf("Error transfering message\n");
		return 0;
	}
	// read feedback
	res = libusb_bulk_transfer(crazyradioPtr->handle, 0x81, data, 7, &numBytes, 1000);
	if (0 != res)
	{
		printf("Error receiving message\n");
		return 0;
	}

	radioAck_init(ackIn);
	if(data[0] != 0){
		ackIn->ack = (data[0] & 0x01) != 0;
		ackIn->powerDet = (data[0] & 0x02) != 0;
		ackIn->retry = data[0] >> 4;//
		strncpy(ackIn->data, &data[1], 63);
	}else{
		ackIn->retry = crazyradioPtr->arc;
	}
	return 1;
}

// Private utility functions
int _send_vendor_setup(libusb_device_handle * 	dev_handle,
uint8_t 	bRequest,
uint16_t 	wValue,
uint16_t 	wIndex,
unsigned char * 	data,
uint16_t 	wLength
){
	//if pyusb1
	return libusb_control_transfer(dev_handle, USB_TYPE_VENDOR, bRequest, wValue, wIndex, data,
	wLength, 1000);
}

int _get_vendor_setup(libusb_device_handle *dev_handle,
//uint8_t 	bmRequestType,
uint8_t 	bRequest,
uint16_t 	wValue,
uint16_t 	wIndex,
unsigned char *data,
uint16_t	wLength
){
	return libusb_control_transfer(dev_handle, USB_TYPE_VENDOR | 0x80, bRequest, wValue, wIndex, data,
	wLength, 1000);
}

void errorReport(int error){
	switch(error){
		case 0:
			printf("operation success\n");
			break;
		case LIBUSB_ERROR_NO_MEM:
			printf("on memory allocation failure\n");
			break;
		case LIBUSB_ERROR_ACCESS:
			printf("insufficient permissions\n");
			break;
		case LIBUSB_ERROR_NO_DEVICE:
			printf("device has been disconnected\n");
			break;
		case LIBUSB_ERROR_NOT_FOUND:
			printf("interface not exist\n");
			break;
		case LIBUSB_ERROR_BUSY:
			printf("driver has claimed by another program\n");
			break;
	}
}