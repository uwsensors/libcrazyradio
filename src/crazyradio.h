#ifndef CRAZYRADIO_H
#define CRAZYRADIO_H

#include <errno.h>
#include <signal.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <libusb.h>
#include <assert.h>

struct _radio_ack
{
	unsigned int ack;
	unsigned int powerDet;
	int retry;
	unsigned char data[64];
}; 
typedef struct _radio_ack radioAck;

struct Crazyradio
{
	// Used for communication with the Crazyradio USB dongle
    // configuration constants
    int DR_250KPS;
    int DR_1MPS;
    int DR_2MPS;

    int P_M18DBM;
    int P_M12DBM;	
    int P_M6DBM;
    int P_0DBM;

    uint16_t version;
    uint16_t arc;

   	struct libusb_device_handle* handle;
   	struct libusb_device* dev;

}; typedef struct Crazyradio crazyradio;

extern void errorReport(int error);
extern void radioAck_init(radioAck *ackPtr);
extern int Crazyradio_USB_init(crazyradio *crazyradioPtr, uint16_t devid);
extern void Crazyradio_init(crazyradio *crazyradioPtr);
extern void close_crazyradio(crazyradio *crazyradioPtr);
extern void set_channel(crazyradio *crazyradioPtr, uint16_t channel);
extern int set_address(crazyradio *crazyradioPtr, unsigned char *address);
extern void set_data_rate(crazyradio *crazyradioPtr, uint16_t datarate);
extern void set_power(crazyradio *crazyradioPtr, uint16_t power);
extern void set_arc(crazyradio *crazyradioPtr, uint16_t arc);
extern void set_ard_time(crazyradio *crazyradioPtr, uint16_t us);
extern void set_ard_bytes(crazyradio *crazyradioPtr, uint16_t nbytes);
extern void set_cont_carrier(crazyradio *crazyradioPtr, int active);
extern int _has_fw_scan(crazyradio *crazyradioPtr);
extern void set_channels(crazyradio *crazyradioPtr, uint16_t channel);
extern void scan_channels(crazyradio *crazyradioPtr, uint16_t start, uint16_t stop,
					unsigned char *packet, uint16_t wLength, unsigned char *array);
extern int send_packet(crazyradio *crazyradioPtr, unsigned char dataOut[],
				radioAck *ackIn);
extern int _send_vendor_setup(libusb_device_handle * 	dev_handle, uint8_t bRequest,
						uint16_t wValue, uint16_t wIndex, unsigned char * data,
						uint16_t wLength);
extern int _get_vendor_setup(libusb_device_handle *dev_handle, uint8_t bRequest, uint16_t 	wValue,
					uint16_t wIndex, unsigned char *data, uint16_t wLength);


#endif //CRAZYRADIO_H